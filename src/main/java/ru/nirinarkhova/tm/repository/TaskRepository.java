package ru.nirinarkhova.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.api.repository.ITaskRepository;
import ru.nirinarkhova.tm.model.Task;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    public static Predicate<Task> predicateByProjectId(final String projectId) {
        return s -> projectId.equals(s.getProjectId());
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull String projectId) {
        return entities.stream()
                .filter(predicateByUserId(userId))
                .filter(predicateByProjectId(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull String projectId) {
        findAllByProjectId(userId, projectId).forEach(entities::remove);
    }

    @NotNull
    @Override
    public Optional<Task> bindTaskToProject(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId) {
        @NotNull final Optional<Task> task = findById(userId, taskId);
        if (!task.isPresent()) return Optional.empty();
        task.get().setProjectId(projectId);
        return task;
    }

    @NotNull
    @Override
    public Optional<Task> unbindTaskFromProject(@NotNull final String userId, @NotNull final String taskId) {
        final Optional<Task> task = findById(userId, taskId);
        if (!task.isPresent()) return Optional.empty();
        task.get().setProjectId(null);
        return task;
    }

}

