package ru.nirinarkhova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    AbstractCommand getCommandByArg(String arg);

    @NotNull
    AbstractCommand getCommandByName(String name);

    void add(@Nullable AbstractCommand command);

}
