package ru.nirinarkhova.tm.api;

import ru.nirinarkhova.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
