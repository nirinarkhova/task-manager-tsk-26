package ru.nirinarkhova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.api.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperName();

}

