package ru.nirinarkhova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @Nullable
    AbstractCommand getCommandByArg(@Nullable String arg);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    void add(AbstractCommand command);

}