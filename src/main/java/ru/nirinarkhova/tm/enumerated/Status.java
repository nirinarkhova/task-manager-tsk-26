package ru.nirinarkhova.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETE("Complete");

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    private String displayName;

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    public static void main(String[] args) {
        System.out.println((COMPLETE));
    }

}
