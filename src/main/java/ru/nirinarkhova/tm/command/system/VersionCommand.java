package ru.nirinarkhova.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.exception.system.UnknownArgumentException;

public class VersionCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "show application version.";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) throw new UnknownArgumentException();
        System.out.println("[VERSION:]");
        System.out.println(Manifests.read("version"));
    }

}

