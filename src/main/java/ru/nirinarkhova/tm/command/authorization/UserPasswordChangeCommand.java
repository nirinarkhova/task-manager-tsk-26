package ru.nirinarkhova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.model.User;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class UserPasswordChangeCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "change-password";
    }

    @NotNull
    @Override
    public String description() {
        return "change password for current user";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("[ENTER NEW PASSWORD:]");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        serviceLocator.getUserService().setPassword(userId, newPassword);
    }

}
