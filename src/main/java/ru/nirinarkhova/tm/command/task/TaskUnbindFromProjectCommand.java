package ru.nirinarkhova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-unbind-from-project";
    }

    @NotNull
    @Override
    public String description() {
        return "unbind task from project.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UNBIND TASK]");
        System.out.println("[ENTER TASK ID]");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final Optional<Task> task = serviceLocator.getProjectTaskService().unbindTaskFromProject(userId, taskId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}

