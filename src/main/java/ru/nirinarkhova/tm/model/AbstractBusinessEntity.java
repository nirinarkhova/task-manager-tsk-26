package ru.nirinarkhova.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
public abstract class AbstractBusinessEntity extends AbstractEntity {

    @NotNull
    protected String userId;

    @Nullable
    protected String name = "";

    @Nullable
    protected String description = "";

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Nullable
    protected Date dateStart;

    @Nullable
    protected Date dateFinish;

    @NotNull
    private Date created = new Date();

    @NotNull
    public String toString() {
        return id + ": " + name + " - " + description;
    }

}

