package ru.nirinarkhova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.api.entity.IWBS;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractBusinessEntity implements IWBS {

    @Nullable
    private String projectId;

    public Task(@Nullable final String name) {
        this.name = name;
    }

}
